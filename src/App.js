import React from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Businnes from './pages/Business'
import Owner from './pages/Owner';
import Loan from './pages/Loan';

const App = () => (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Businnes}/>
        <Route exact path="/businessForm" component={Businnes}/>
        <Route exact path="/owner" component={Owner}/>
        <Route exact path="/loan" component={Loan}/>
        <Route component={Businnes}/>
      </Switch>
    </BrowserRouter>
)

export default App;
