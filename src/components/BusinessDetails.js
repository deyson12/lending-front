import React from 'react'

const BusinessDetails = ({business}) => (
        <React.Fragment>
        {business ? (
            <React.Fragment>
                <table id="business">
                    <thead>
                        <tr>
                            <th colSpan="2">Business Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Tax id</td>
                            <td>{business.taxId}</td>
                        </tr>
                        <tr>
                            <td>Business Name</td>
                            <td>{business.name}</td>
                        </tr>
                        <tr>
                            <td>Business Address</td>
                            <td>{business.address}</td>
                        </tr>
                        <tr>
                            <td>City</td>
                            <td>{business.city}</td>
                        </tr>
                        <tr>
                            <td>State</td>
                            <td>{business.state}</td>
                        </tr>
                        <tr>
                            <td>Postal Code</td>
                            <td>{business.postalCode}</td>
                        </tr>
                        <tr>
                            <td>Requested amount</td>
                            <td>$ {business.requestedAmout} COP</td>
                        </tr>
                    </tbody>
                </table>
            </React.Fragment>
        ) : (
            <span></span>
        )}
        </React.Fragment>
)

export default BusinessDetails