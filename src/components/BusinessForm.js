import React from 'react'

class Business extends React.Component {
    
    state = {}

    render(){
        const { onChange, form } = this.props
        return (
        <form>
        <div id="container">
            <div className="row">
                <div className="col-25">
                    <label htmlFor="formatName">Tax id</label>
                </div>
                <div className="col-75">
                    <input type="text" 
                        name="taxId"
                        onChange={onChange}
                        value={form.taxId}
                    />
                </div>
            </div>
            <div className="row">
                <div className="col-25">
                    <label htmlFor="formatName">Business Name</label>
                </div>
                <div className="col-75">
                    <input type="text"
                        name="name"
                        onChange={onChange}
                        value={form.name}
                    />
                </div>
            </div>
            <div className="row">
                <div className="col-25">
                    <label htmlFor="formatName">Business Address</label>
                </div>
                <div className="col-75">
                    <input type="text"
                        name="address"
                        onChange={onChange}
                        value={form.address}
                    />
                </div>
            </div>
            <div className="row">
                <div className="col-25">
                    <label htmlFor="formatName">City</label>
                </div>
                <div className="col-75">
                    <input type="text"
                        name="city"
                        onChange={onChange}
                        value={form.city}
                    />
                </div>
            </div>
            <div className="row">
                <div className="col-25">
                    <label htmlFor="formatName">State</label>
                </div>
                <div className="col-75">
                    <input type="text"
                        name="state"
                        onChange={onChange}
                        value={form.state}
                    />
                </div>
            </div>
            <div className="row">
                <div className="col-25">
                    <label htmlFor="formatName">Postal Code</label>
                </div>
                <div className="col-75">
                    <input type="text"
                        name="postalCode"
                        onChange={onChange}
                        value={form.postalCode}
                    />
                </div>
            </div>
            <div className="row">
                <div className="col-25">
                    <label htmlFor="formatName">Requested amount</label>
                </div>
                <div className="col-75">
                    <input type="number"
                        name="requestedAmout"
                        onChange={onChange}
                        value={form.requestedAmout}
                    />
                </div>
            </div>
        </div>
        </form>
        )
    }
}

export default Business