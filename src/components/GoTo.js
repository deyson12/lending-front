import React from 'react'
import { Link } from 'react-router-dom'

class GoTo extends React.Component{
    render(){
        const {path, text, business} = this.props
        return(
        <React.Fragment>
            <Link className={text == 'Back' ? 'goTo2' : 'goTo'} to={{
                pathname: path,
                state: {
                    business
                }
                }}>{text}</Link>&nbsp;
        </React.Fragment>
        )
    }
}

export default GoTo