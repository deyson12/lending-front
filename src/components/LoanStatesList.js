import React from 'react'

import Loading from './loading'
import Error404 from '../pages/Error404'

class LoanStatesList extends React.Component {

    state = {
        data: [],
        loading: true,
        error: null
    }
    
    async componentDidMount(){
        await this.fetchLoanInfo()
    }

    fetchLoanInfo = async () => {

        try {
            let res = await fetch('https://back-lending-front.herokuapp.com/loan')
            let data = await res.json()

            console.table(data)

            this.setState({
                data,
                loading: false
            })
        } catch (error) {
            this.setState({
                loading: false,
                error
            })
        }
        
    }

    render(){
        if(this.state.loading)
            return <Loading/>

        if(this.state.error)
            return <Error404/>

        return(
            <React.Fragment>
                <table id="business">
                            <thead>
                            <tr>
                                <th colSpan="3">Loan payments</th>
                            </tr>
                            <tr>
                                <th>Id transaction</th>
                                <th>Value</th>
                                <th>State</th>
                            </tr>
                        </thead>
                        <tbody>
                    { this.state.data.map((loan) => {
                        return(
                        <tr>
                            <td>{loan.idTransaction}</td>
                            <td>$ {loan.value} COP</td>
                            <td>{loan.state}</td>
                        </tr>
                        )
                    })
                    }
                    </tbody>
                        </table>
            </React.Fragment>
        )
    }

}

export default LoanStatesList