import React from 'react'

import lendingFronImg from '../images/lending-front.png'
import BusinessDetails from './BusinessDetails'

const Logo = ({business}) => (
    <React.Fragment>
        <div className="column">
            <img className="logo" src={lendingFronImg}/>
            <BusinessDetails
            business={business}
        />
        </div>
    </React.Fragment>
)

export default Logo