import React from 'react'

class OwnerForm extends React.Component {
    
    state = {
        texto: 'Owner Form'
    }

    validate(form) {
        const isEnabled = 
            form.socialSecurityNumber &&
            form.name &&
            form.email &&
            form.address &&
            form.city &&
            form.state &&
            form.postalCode

        return isEnabled
    }

    render(){
        const { onChange, onSubmit, form } = this.props
        const isEnabled = this.validate(form);
        return (
            <form onSubmit={onSubmit}>
                <div id="container">
                    <div className="row">
                        <div className="col-25">
                            <label htmlFor="formatName">Social Security Number</label>
                        </div>
                        <div className="col-75">
                            <input type="text" 
                                name="socialSecurityNumber"
                                onChange={onChange}
                                value={form.socialSecurityNumber}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-25">
                            <label htmlFor="formatName">Name</label>
                        </div>
                        <div className="col-75">
                            <input type="text" 
                                name="name"
                                onChange={onChange}
                                value={form.name}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-25">
                            <label htmlFor="formatName">Email</label>
                        </div>
                        <div className="col-75">
                            <input type="text" 
                                name="email"
                                onChange={onChange}
                                value={form.email}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-25">
                            <label htmlFor="formatName">Address</label>
                        </div>
                        <div className="col-75">
                            <input type="text" 
                                name="address"
                                onChange={onChange}
                                value={form.address}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-25">
                            <label htmlFor="formatName">City</label>
                        </div>
                        <div className="col-75">
                            <input type="text" 
                                name="city"
                                onChange={onChange}
                                value={form.city}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-25">
                            <label htmlFor="formatName">State</label>
                        </div>
                        <div className="col-75">
                            <input type="text" 
                                name="state"
                                onChange={onChange}
                                value={form.state}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-25">
                            <label htmlFor="formatName">Postal code</label>
                        </div>
                        <div className="col-75">
                            <input type="text" 
                                name="postalCode"
                                onChange={onChange}
                                value={form.postalCode}
                            />
                        </div>
                    </div>
                    <div className="row">
                        <button 
                            disabled={!isEnabled ? 'disabled': ''} 
                            className={!isEnabled ? 'sendDisabled': 'send'} 
                            type="submit"> Send info</button>
                    </div>
                </div>
            </form>
        )
    }
}

export default OwnerForm