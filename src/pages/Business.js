import React from 'react'

import Tittle from '../components/tittle'
import BusinessForm from '../components/BusinessForm'
import GoTo from '../components/GoTo'
import '../components/styles/General.css'
import Logo from '../components/Logo'

class Business extends React.Component {
    
    state = {
        form: {
            taxId: null,
            name: '',
            address: '',
            city: '',
            state: '',
            postalCode: '',
            requestedAmout: null
        }
    }

    componentDidMount(){
        if (this.props.location.state){
            const {business} = this.props.location.state || null

            this.setState({
                business
            })
        }
    }

    handleChange = e => {
        this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value
            }
        })
    }

    validate(form) {
        const isEnabled = 
            form.taxId &&
            form.name &&
            form.address &&
            form.city &&
            form.state &&
            form.postalCode &&
            form.requestedAmout
            
        return isEnabled
    }
    
    render(){
        const isEnabled = this.validate(this.state.form);
        return(
            <div>
                <Tittle tittleDescription="Business Information"/>
                <Logo/>
                <div className="column">
                    <BusinessForm 
                        onChange={this.handleChange}
                        form={this.state.form}/>
                </div>
                <div className="divButton">
                {   isEnabled ?
                    <GoTo 
                        path="/owner" 
                        text="Next"
                        business={this.state.form}
                    /> : ''
                }
                </div>
            </div>
        )
    }
}

export default Business