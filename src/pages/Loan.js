import React from 'react'
import LoanStatesList from '../components/LoanStatesList'
import BusinessDetails from '../components/BusinessDetails'
import GoTo from '../components/GoTo'

class Owner extends React.Component {

    state = {
        form: {},
        requestedAmout: 0,
        business: null,
        error: null
    }

    componentDidMount(){
        if (this.props.location.state){
            const {business} = this.props.location.state || null

            this.setState({
                business
            })
        }
    }

    render(){
        return(
            <React.Fragment>
                <BusinessDetails
                    business={this.state.business}
                />
                <LoanStatesList/>
                <GoTo 
                    path="/owner"
                    text="Back"
                    business={this.state.business}
                    />
            </React.Fragment>
        )
    }
}

export default Owner