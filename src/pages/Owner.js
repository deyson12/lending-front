import React from 'react'

import OwnerForm from '../components/OwnerForm'
import GoTo from '../components/GoTo'
import BusinessDetails from '../components/BusinessDetails'
import Error404 from '../pages/Error404'
import Loading from '../components/loading'
import Logo from '../components/Logo'
import Tittle from '../components/tittle'

class Owner extends React.Component {

    state = {
        form: {
            socialSecurityNumber: null,
            name: '',
            email: '',
            address: '',
            city: '',
            state: '',
            postalCode: ''
        },
        requestedAmout: 0,
        business: null,
        loading: false,
        error: null,
        active: false,
        response: {
            status: 'INIT'
        }
    }

    componentDidMount(){
        if (this.props.location.state){
            const {business} = this.props.location.state || null

            this.setState({
                business
            })
        }
    }

    handleChange = e => {
        this.setState({
            form: {
                ...this.state.form,
                [e.target.name]: e.target.value
            }
        })
    }

    handleSubmit = async e => {
        e.preventDefault()

        this.setState({
            loading: true
        })

        try {
            let config = {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(
                    {
                        owner: this.state.form,
                        business: this.state.business
                    }
                )
            }

            let res = await fetch('https://back-lending-front.herokuapp.com/business', config)
            let json = await res.json()
            
            this.setState({
                loading: false,
                active: true,
                response: json
            })

             console.log(json);
        } catch (error) {
            this.setState({
                loading: false,
                error
            })
        }
    }
    
    render(){
        const state = this.state

        if(state.loading)
            return <Loading/>

        if(state.error)
            return <Error404/>

        return(
            <React.Fragment>
                <Tittle tittleDescription="Owner Information"/>
                <Logo business={state.business}/>
                <div className="column">
                {
                    state.response && state.response.status != 'INIT' ? 
                    <div id="alert" className={
                        state.response.status == 'APROV' ? "okMsg" : 
                        (state.response.status == 'UNDE' ? "warnMsg" : "errorMsg")
                        }>
                        <strong>{state.response.status}!</strong> {state.response.msg} COP.
                    </div>
                    : ''
                }
                <OwnerForm 
                    onChange={this.handleChange}
                    onSubmit={this.handleSubmit}
                    form={state.form}
                />
                <div className="divButton">
                {
                    state.response.status == 'APROV' ? 
                    <GoTo 
                        path="/loan"
                        text="Next"
                        business={state.business}
                    /> 
                    : ''
                }
                &nbsp;
                <GoTo 
                    path="/business"
                    text="Back"
                    business={state.business}
                />
                </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Owner